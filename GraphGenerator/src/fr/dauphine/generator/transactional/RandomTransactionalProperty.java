package fr.dauphine.generator.transactional;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.service.CompositeTransactionalProperty;
import fr.dauphine.service.Service;
import fr.dauphine.service.TransactionalProperty;

public class RandomTransactionalProperty {
	
	/**
	 * Randomly assigns a transactional property to a composite Web services and its services.
	 * The possible transactional properties are: Atomic, Atomic-Retriable, Compensable, and Compensable-Retriable.
	 * 
	 * - Atomic: there exists one and only one service with TP = pivot; the TP of all its predecessors is 
	 *   compensable or compensable-retriable, the TP of all its successors is compensable-retriable or pivot-retriable.
	 *   All its parallel services must be compensable-retriable.
	 * - Atomic-Retriable: there exists at least one service with TP = pivot-retriable; for all other services, TP = pivot-retriable or TP = compensable-retriable
	 * - Compensable: there exists at least one service with TP = compensable; for all other services, TP = compensable or TP = compensable-retriable
	 * - Compensable-Retriable: for all services, TP = compensable-retriable
	 * 
	 * For more information, read the paper:
	 * TQoS: Transactional and QoS-aware selection algorithm for automaticWeb service composition
	 * @param graph: the graph representing the composite Web service.
	 */
	public static <E> void generateRandTP(Graph<Service, E> graph) {
		
		CompositeTransactionalProperty tp = CompositeTransactionalProperty.randomCompositeTP();
		//tp = CompositeTransactionalProperty.Atomic;
		Random r = new Random();
		
		switch (tp) {
		case Atomic:
			
			Service p = getRandomService(graph);
			GraphUtils.setTransactionalProperty(graph, p, TransactionalProperty.Pivot);
			
			List<Service> notVisited = new ArrayList<Service>(graph.getVertices());
			
			//predecessors
			List<TransactionalProperty> availableTPpred = new ArrayList<TransactionalProperty>();
			availableTPpred.add(TransactionalProperty.Compensable);
			availableTPpred.add(TransactionalProperty.CompensableRetriable);
			
			
			Queue<Service> queue = new LinkedList<Service>() ;
				
			queue.clear();
		    queue.add(p);
		    while(!queue.isEmpty()){
		        Service node = queue.remove();
		        notVisited.remove(node);
		        if(!node.equals(p))
		        	GraphUtils.setTransactionalProperty(graph, node
		        			, availableTPpred.get(r.nextInt(availableTPpred.size())));
		        
		       for(Service pred:graph.getPredecessors(node))
		    	   queue.add(pred);
		    }
		    
		    //successors
			List<TransactionalProperty> availableTPsucc = new ArrayList<TransactionalProperty>();
			availableTPsucc.add(TransactionalProperty.PivotRetriable);
			availableTPsucc.add(TransactionalProperty.CompensableRetriable);
			
			
			queue = new LinkedList<Service>() ;
				
			queue.clear();
		    queue.add(p);
		    while(!queue.isEmpty()){
		        Service node = queue.remove();
		        notVisited.remove(node);
		        if(!node.equals(p))
		        	GraphUtils.setTransactionalProperty(graph, node
		        			, availableTPsucc.get(r.nextInt(availableTPsucc.size())));
		        
		       for(Service pred:graph.getSuccessors(node))
		    	   queue.add(pred);
		    }
			
		    //parallel
		    for(Service s:notVisited)
		    	GraphUtils.setTransactionalProperty(graph, s, TransactionalProperty.CompensableRetriable);
		    
			
			break;
			
		case AtomicRetriable:
			
			Service ar = getRandomService(graph);
			GraphUtils.setTransactionalProperty(graph, ar, TransactionalProperty.PivotRetriable);
			
			List<TransactionalProperty> availableTPar = new ArrayList<TransactionalProperty>();
			availableTPar.add(TransactionalProperty.PivotRetriable);
			availableTPar.add(TransactionalProperty.CompensableRetriable);
			
			for(Service s:graph.getVertices())
				if(!s.equals(ar))
					GraphUtils.setTransactionalProperty(
							graph, s, availableTPar.get(r.nextInt(availableTPar.size()))
								);
				
			break;
			
		case Compensable:
			
			Service c = getRandomService(graph);
			GraphUtils.setTransactionalProperty(graph, c, TransactionalProperty.Compensable);
			
			List<TransactionalProperty> availableTP = new ArrayList<TransactionalProperty>();
			availableTP.add(TransactionalProperty.Compensable);
			availableTP.add(TransactionalProperty.CompensableRetriable);
			
			for(Service s:graph.getVertices())
				if(!s.equals(c))
					GraphUtils.setTransactionalProperty(
							graph, s, availableTP.get(r.nextInt(availableTP.size()))
								);
			
			break;
			
			
		case CompensableRetriable:
			
			for(Service s:graph.getVertices())
				GraphUtils.setTransactionalProperty(graph, s, TransactionalProperty.CompensableRetriable);
			
			break;

		default:
			break;
		}
		
	}
	
	private static <E> Service getRandomService(Graph<Service, E> graph) {
		Random r = new Random();
		return (Service) graph.getVertices().toArray()[r.nextInt(graph.getVertices().size())];
	}
	

}
