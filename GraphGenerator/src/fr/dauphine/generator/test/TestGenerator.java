package fr.dauphine.generator.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.generator.GraphGenerator;
import fr.dauphine.generator.transactional.RandomTransactionalProperty;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.service.Service;
import fr.dauphine.service.TransactionalProperty;

public class TestGenerator {
	
	public static void main(String[] args) {
		
		/*for(int i=1; i<=1000; i++) {
			System.out.println(i);
			(new GraphGenerator<Integer,Number>()).generate(BarabasiAlbertGenerator.class, new Integer(i), new Integer(5));
		}*/
		
		Result result = JUnitCore.runClasses(TestGenerator.class);
	    for (Failure failure : result.getFailures()) {
	      System.out.println(failure.toString());
	    }
		
	}

	
	@Test
	public void generateRandTP() {
		
		GraphGenerator<Service, Number> generator = new GraphGenerator<Service, Number>();
		Graph<Service, Number> graph = 
				GraphUtils.toServiceGraph(generator.generateRandomBarabasiAlbert(5));
		
		RandomTransactionalProperty.generateRandTP(graph);
		
		for(Service s:graph.getVertices())
			assertEquals("All services must have a TP"
					, false, s.getTransactionalProperty().equals(TransactionalProperty.None));
	}
}
