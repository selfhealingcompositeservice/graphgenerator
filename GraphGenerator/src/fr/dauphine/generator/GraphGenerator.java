package fr.dauphine.generator;
import java.util.HashSet;
import java.util.List;




import org.apache.commons.collections15.Factory;

import edu.uci.ics.jung.algorithms.generators.random.BarabasiAlbertGenerator;
import edu.uci.ics.jung.algorithms.generators.random.KleinbergSmallWorldGenerator;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Pair;
import fr.dauphine.graphreader.GraphWriter;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.service.Service;

/**
* This class generates random graphs.
* @author Rafael Angarita
* @version 1.0
*/

public class GraphGenerator<V, E> {
	
	Factory<Number> edgeFactory;
	int mNumEdgesToAttachPerStep;
	Graph<Integer, Number> graph;
	Class<?> graphType;

	public static void main(String[] args) {
		System.out.println(args[0]); 
		(new GraphGenerator<Integer,Number>()).generate(BarabasiAlbertGenerator.class, new Integer(args[1]), new Integer(args[2]));
		
	}
	
	public static Graph<Service, Number> generateGraph(List<Pair<String>> edges) {
		
		Factory<Graph<Service,Number>> graphFactory =
	        	new Factory<Graph<Service,Number>>() {
	        	public Graph<Service,Number> create() {
	        		return new DirectedSparseMultigraph<Service,Number>();
	        	}
	        };
		
		Graph<Service, Number> graph = graphFactory.create();
		int n = 0;
				
		for(Pair<String> p:edges)
			graph.addEdge(n++, 
					new Service(p.getFirst()), 
					new Service(p.getSecond()));
		
		
		 return graph;
	}
	
	public Graph<Integer, Number> generateRandomBarabasiAlbert(int vertexCount) {
		
		int init_vertices = 1;
		mNumEdgesToAttachPerStep = 2;
        int random_seed = 0;
        //int num_tests = 10;
        int num_timesteps = vertexCount-1;
		
		Factory<Graph<Integer,Number>> graphFactory =
        	new Factory<Graph<Integer,Number>>() {
        	public Graph<Integer,Number> create() {
        		return new DirectedSparseMultigraph<Integer,Number>();
        	}
        };
    	Factory<Integer> vertexFactory = 
    		new Factory<Integer>() {
    			int count;
				public Integer create() {
					return count++;
				}};
				
		edgeFactory = 
		    new Factory<Number>() {
			    int count;
				public Number create() {
					return count++;
				}};
		BarabasiAlbertGenerator<Integer,Number> generator = 
	            new BarabasiAlbertGenerator<Integer,Number>(graphFactory, vertexFactory, edgeFactory,
	            		init_vertices,mNumEdgesToAttachPerStep,random_seed, new HashSet<Integer>());

		generator.evolveGraph(num_timesteps);
        graph = generator.create();
        
        return graph;
	}
	
	public <T> Graph<Integer, Number> generate(Class<T> graphType, int vertexNumber, int conectionNumber) {
		this.graphType = graphType;
		try {
		
			int init_vertices = 1;
			mNumEdgesToAttachPerStep = conectionNumber;
	        int random_seed = 0;
	        //int num_tests = 10;
	        int num_timesteps = vertexNumber-1;
	        
	        Factory<Graph<Integer,Number>> graphFactory =
	        	new Factory<Graph<Integer,Number>>() {
	        	public Graph<Integer,Number> create() {
	        		//return new SparseMultigraph<Integer,Number>();
	        		return new DirectedSparseMultigraph<Integer,Number>();
	        		//return new DirectedSparseGraph<Integer,Number>();
	        		
	        	}
	        };
	    	Factory<Integer> vertexFactory = 
	    		new Factory<Integer>() {
	    			int count;
					public Integer create() {
						return count++;
					}};
					
			edgeFactory = 
			    new Factory<Number>() {
				    int count;
					public Number create() {
						return count++;
					}};
					
			//System.out.println(graphType.equals(BarabasiAlbertGenerator.class));
			//Barabasi-Albert
		    if(graphType.equals(BarabasiAlbertGenerator.class)) {
			    BarabasiAlbertGenerator<Integer,Number> generator = 
		            new BarabasiAlbertGenerator<Integer,Number>(graphFactory, vertexFactory, edgeFactory,
		            		init_vertices,mNumEdgesToAttachPerStep,random_seed, new HashSet<Integer>());
			    //for (int i = 1; i <= num_tests; i++) {
			    generator.evolveGraph(num_timesteps);
		        graph = generator.create();
		    } else if (graphType.equals(KleinbergSmallWorldGenerator.class)) {
		        
		    	KleinbergSmallWorldGenerator <Integer,Number> generator = new KleinbergSmallWorldGenerator<Integer,Number>(graphFactory, vertexFactory, edgeFactory,10,1);
		    	graph = generator.create();
		    }
		    
		        
	
		        //System.out.println(graph);
		  
		        //assertEquals(graph.getVertexCount(), (i*num_timesteps) + init_vertices);
		        //assertEquals(graph.getEdgeCount(), edges_to_add_per_timestep * (i*num_timesteps));
		    //}

		    //add cycle to verify
		    //graph.addEdge(243320, 1, 3);
		    
		    
		    //List<V> ll = new ArrayList(graph.getVertices());
		    //createRandomEdge(ll, (V) vertexFactory.create(), random_seed, (Graph<V, E>) graph);
		    
		    
		    //check for parallel edges
		    //System.out.println("Edges");
		    for(Number e: graph.getEdges()) {
		    	
		    	//System.out.println(e);
		    	Pair<Integer> ep = graph.getEndpoints(e);
		    	
		    	 for(Number e2: graph.getEdges()) {
		    		 
		    		 Pair<Integer> ep2 = graph.getEndpoints(e2);
		    		 
		    		 if(e.intValue() != e2.intValue() && ep.equals(ep2)) {
		    			 throw new Exception("exception: " + "graph contains multi edges");
		    			 
		    		 }
		    	 }
		    	
		    }
		    
		    //check for cycles
		    if(GraphUtils.hasCycles(graph))
		    	throw new Exception("The graph has cycles!");
		    
		    //visualizeGraph(graph);
		    
		    GraphWriter.writeGraph(graph, "BA_" + vertexNumber + "_" + conectionNumber);
		    
		    
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return graph;
			
	}
	
	/*private void createRandomEdge(List<V> preexistingNodes,
			V newVertex, int seed, Graph<V,E> graph) {
        V attach_point;
        boolean created_edge = false;
        Pair<V> endpoints;
        
        Random mRandom = new Random(seed);
        
        graph.addVertex(newVertex);
        
        for(int i = 0; i < mNumEdgesToAttachPerStep; i++) {
        
	        do {
	            //attach_point = vertex_index.get(mRandom.nextInt(vertex_index.size()));
	        	attach_point = preexistingNodes.get(mRandom.nextInt(preexistingNodes.size()));
	            
	            endpoints = new Pair<V>(newVertex, attach_point);
	           
	
	            double degree = graph.inDegree(attach_point);
	            
	            // subtract 1 from numVertices because we don't want to count newVertex
	            // (which has already been added to the graph, but not to vertex_index)
	            double attach_prob = (degree + 1) / (graph.getEdgeCount() + graph.getVertexCount() - 1);
	            if (attach_prob >= mRandom.nextDouble())
	                created_edge = true;
	        }
	        while (!created_edge);
	        
	        
	        if(!graph.isNeighbor(endpoints.getFirst(), endpoints.getSecond()))
	        	graph.addEdge((E) edgeFactory.create(), endpoints.getFirst(), endpoints.getSecond());
        }
    }*/
	

}
