package fr.dauphine.graphreader;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

import edu.uci.ics.jung.graph.Graph;

/**
* This class writes a graph into a fille.
* @author Rafael Angarita
* @version 1.0
*/

public class GraphWriter {
	
	final static Charset ENCODING = StandardCharsets.UTF_8;
	
	public static void writeGraph(Graph<?, Number> graph, String filename) {
		//System.out.println(graph);
		
		PrintWriter writer;
		try {
			writer = new PrintWriter(filename, ENCODING.toString());
			//writer.println( + " v=" + graph.getVertexCount() + " connection=" + mNumEdgesToAttachPerStep);
			writer.println("Vertices");
			Iterator<?> i = graph.getVertices().iterator();
			
			for(int j=0; j<graph.getVertices().size(); j++)
				writer.println(i.next());
				
			writer.println("Edges");
			for(Number e:graph.getEdges())
				writer.println(e.toString() + graph.getEndpoints(e));
			
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

}