package fr.dauphine.graphreader;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.graph.util.Pair;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.query.Query;
import fr.dauphine.service.Service;
import fr.dauphine.utils.Utils;

/**
* This class reads a graph from a fille.
* @author Rafael Angarita
* @version 1.0
*/

public class GraphReader {
	
	final static Charset ENCODING = StandardCharsets.UTF_8;
	
	
	/**
	 * Returns the list of services name contained in the given graph
	 * @param  filename  the file containing the graph
	 * @return the list of services in the graph
	 */
	public static List<String> getServices(String filename) {
		
		List<String> services = new ArrayList<String>();
		
		Path path = Paths.get(filename);
	    try (Scanner scanner =  new Scanner(path, ENCODING.name())){
	    	if(scanner.hasNextLine())
	    		scanner.nextLine();
	    	while (scanner.hasNextLine()){
	    		//process each line in some way
	    		String line = scanner.nextLine();
	    		if(line.compareTo("Edges") == 0)
	    			break;
	    		if(line.compareTo("Vertices") == 0)
	    			continue;
	    		services.add(line);
	    	}      
	    } catch (IOException e) {
			e.printStackTrace();
		}
		
		return services;
	}
	
	/**
	 * Returns the the JUNG graph contained in the given file
	 * @param  filename  the file containing the graph
	 * @return the list of services in the graph
	 */
	public static Graph<Integer, Number> getGraph(String filename) {
		
		Graph<Integer, Number> graph = new DirectedSparseGraph<>();
		
		Path path = Paths.get(filename);
		System.out.println(path.toAbsolutePath());
	    try (Scanner scanner =  new Scanner(path, ENCODING.name())){
	    	while (scanner.hasNextLine()){
	    		String line = scanner.nextLine();
	    		if(line.compareTo("Edges") == 0)
	    			break;
	    	}
	    	
	    	while (scanner.hasNextLine()){
	    		//process each line in some way
	    		String line = scanner.nextLine();
	    		//edges of type: number<integer, integer>
	    	      Pattern p = Pattern.compile("([0-9.,]+)(?:<)([0-9.,]+)(?:,\\s*)([0-9.,]+)(?:>)");
	    	      Matcher m = p.matcher(line );
	    	      if( m.matches()) { 
	    	         
	    	    	  graph.addEdge(new Integer(m.group(1)),new Integer( m.group(2)),  new Integer(m.group(3)), EdgeType.DIRECTED); 

	    	      }
	    	}
	    	
	    	
	    } catch (IOException e) {
			e.printStackTrace();
		}
	    return graph;
	}
	    public static Graph<Service, Number> getGraphAsServiceGraph(String filename) {
			
	    	List<Service> services = new ArrayList<>();
	    	
			Graph<Service, Number> graph = new DirectedSparseGraph<>();
			
			Path path = Paths.get(filename);
		    try (Scanner scanner =  new Scanner(path, ENCODING.name())){
		    	while (scanner.hasNextLine()){
		    		String line = scanner.nextLine();
		    		if(line.compareTo("Edges") == 0)
		    			break;
		    	}
		    	
		    	while (scanner.hasNextLine()){
		    		//process each line in some way
		    		String line = scanner.nextLine();
		    		//edges of type: number<integer, integer>
		    	      Pattern p = Pattern.compile("([0-9.,]+)(?:<)([0-9.,]+)(?:,\\s*)([0-9.,]+)(?:>)");
		    	      Matcher m = p.matcher(line );
		    	      if( m.matches()) { 
		    	         
		    	    	  graph.addEdge(new Integer(m.group(1)),
		    	    			  addService(m.group(2), services),  
		    	    			  addService(m.group(3), services), 
		    	    			  EdgeType.DIRECTED); 

		    	      }
		    	}
		    	
		    	
		    } catch (IOException e) {
				e.printStackTrace();
			}
		return graph;
	}
	
	static Service addService(String name, List<Service> services) {
		
		for(Service s:services)
			if(s.getName().equals(name))
				return s;
		
		Service s = new Service(name);
		services.add(s);
		return s;
	}
	
	
	/**
	 * Returns the the JUNG graph contained in the given file
	 * @param  filename  the file containing the graph
	 * @return the list of services in the graph
	 */
	public static Graph<Service, Number> getGraph(String filename, String qosFilename) {
		
		
		Graph<Service, Number> graph = new DirectedSparseGraph<>();
		
		
		//add services
		
		List<Service> servicesQoS = getServicesQoS(qosFilename);
		
		
		//build the graph
		Graph<Integer, Number> g = getGraph(filename);
		
		for(Number edge:g.getEdges()) {
			Pair<Integer> endpoints = g.getEndpoints(edge);
			graph.addEdge(edge, 
					getServiceByName(endpoints.getFirst().toString(), servicesQoS), 	
					getServiceByName(endpoints.getSecond().toString(), servicesQoS));
		}
		
		
		return graph;
	}
	
	/**
	 * Returns the the JUNG graph contained in the given file
	 * @param  filename  the file containing the graph
	 * @return the list of services in the graph
	 */
	public static Graph<Service, Number> getGraph(String filename, String qosFilename, String dataFilename) {
		
		Graph<Service, Number> graph = getGraph(filename, qosFilename);
		
		Path path = Paths.get(dataFilename);
	    try (Scanner scanner =  new Scanner(path, ENCODING.name())){
	    	while (scanner.hasNextLine()){
	    		String line = scanner.nextLine();
	    		String [] serviceData = line.split(" ");
	    		Service service = GraphUtils.getVertexByName(graph, serviceData[0]);
	    		service.setInputs(Utils.asDataList(Arrays.asList(getData(serviceData[1]))));
	    		service.setOutputs(Utils.asDataList(Arrays.asList(getData(serviceData[2]))));
	    	}      
	    } catch (IOException e) {
			e.printStackTrace();
		}
		
		return graph;
	}
	
	public static Query getQuery(String filename) {
		Query query = new Query();
		Path path = Paths.get(filename);
	    try (Scanner scanner =  new Scanner(path, ENCODING.name())){
	    	while (scanner.hasNextLine()){
	    		String line = scanner.nextLine();
	    		String [] serviceData = line.split(" ");
	    		query.setInputs(Utils.asDataList(Arrays.asList(getData(serviceData[0]))));
	    		query.setOutputs(Utils.asDataList(Arrays.asList(getData(serviceData[1]))));
	    	}      
	    } catch (IOException e) {
			e.printStackTrace();
		}
		
		return query;
	}
	
	private static String [] getData(String data) {
		return data.substring(data.indexOf(":")+1, data.length()).split(",");
	}
	
	private static Service getServiceByName(String name, List<Service> services) {
		
		for(Service s:services)
			if(s.getName().equals(name))
				return s;
		
		
		return null;
	}
	
	
	/**
	 * Returns the list of services with their QoS
	 * @param  filename  the file containing the graph
	 * @return the list of services in the graph
	 */
	public static List<Service> getServicesQoS(String filename) {
		
		List<Service> services = new ArrayList<Service>();
		
		Path path = Paths.get(filename);
	    try (Scanner scanner =  new Scanner(path, ENCODING.name())){
	    	while (scanner.hasNextLine()){
	    		String line = scanner.nextLine();
	    		String [] lineQoS = line.split(" ");
	    		Service service = new Service(lineQoS[0]);
	    		
	    		//estimated execution time
	    		if(lineQoS.length > 1)
	    			service.setEstimatedExecutionTime(Double.parseDouble(lineQoS[1]));
	    		
	    		//availability
	    		if(lineQoS.length > 2)
	    			service.setAvailability(Double.parseDouble(lineQoS[2]));
	    		
	    		//price
	    		if(lineQoS.length > 3)
	    			service.setPrice(Double.parseDouble(lineQoS[3]));
	    		
	    		
	    		services.add(service);
	    	}      
	    } catch (IOException e) {
			e.printStackTrace();
		}
		
		return services;
	}
}
