package fr.dauphine.graphutils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.collections15.Factory;

import edu.uci.ics.jung.algorithms.filters.KNeighborhoodFilter;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.graph.util.Pair;
import fr.dauphine.query.Query;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;
import fr.dauphine.service.Constants;
import fr.dauphine.service.TransactionalProperty;

/**
* This class contains some methods to manipulate graphs.
* @author Rafael Angarita
* @version 1.0
*/

public class GraphUtils {
	
	public static final String CONCEPT = "concept";
	
	/**
	 * Adds to the specified graph the dummy nodes wsci and wscf. 
	 * wsci and wscf are used as control nodes, and wsci is predecessor
	 * to all "input" nodes and wsci is successor to all "output" nodes.
	 *
	 * @param  graph the graph to be modified with the control nodes
	 */
	public static void addControlNodes(Graph<Service, Number> graph) {
		Service wsci = new Service(Constants.INITIAL_NODE);
		wsci.setEstimatedExecutionTime(0.0);
		
		Service wscf = new Service(Constants.FINAL_NODE);
		wscf.setEstimatedExecutionTime(0.0);

		Service [] services = graph.getVertices().toArray (new Service[graph.getVertices().size()]);
		for(Service s:services) {
			if(graph.getPredecessorCount(s) == 0) {
				addEdge(graph, wsci, s);
				wsci.addInputs(s.getInputs());
			    wsci.addOutputs(s.getInputs());
			} 
			if(graph.getSuccessorCount(s) == 0) {
				addEdge(graph, s, wscf);
			    wscf.addInputs(s.getOutputs());
			    wscf.addOutputs(s.getOutputs());
			}
		}
	}
	
	private static void addEdge(Graph<Service, Number> graph, Service v1, Service v2) {
		Number edge = graph.getEdgeCount()+1;
		while(graph.containsEdge(edge))
			edge = edge.intValue()*2;
		graph.addEdge(edge, v1, v2, EdgeType.DIRECTED);
	}
	
	/**
	 * Adds to the specified graph the dummy nodes <code>wsci</code>  and <code>wscf</code>. 
	 * <code>wsci</code> and <code>wscf</code> are used as control nodes, and <code>wsci</code> is predecessor
	 * to all "input" nodes and <code>wsci</code> is successor to all "output" nodes.
	 *
	 * @param  graph the graph to be modified with the control nodes
	 * @param  q the query containing required inputs and outputs
	 */
	public static void addControlNodes(Graph<Service, Number> graph, Query q) {
		Service wsci = new Service(Constants.INITIAL_NODE);
		wsci.setEstimatedExecutionTime(0.0);
		wsci.setOutputs(q.getInputs());
		
		Service wscf = new Service(Constants.FINAL_NODE);
		wscf.setEstimatedExecutionTime(0.0);
		wscf.setInputs(q.getOutputs());
		
		Service [] services = graph.getVertices().toArray(new Service[graph.getVertices().size()]);
		for(Service s:services) {
			if(listIntersection(s.getInputs(), wsci.getOutputs()).size() != 0)
				graph.addEdge(graph.getEdgeCount()+1, wsci, s, EdgeType.DIRECTED);
			if(listIntersection(s.getOutputs(), wscf.getInputs()).size() != 0)
				graph.addEdge(graph.getEdgeCount()+1, s, wscf, EdgeType.DIRECTED);
		}
		
	}
	
	/**
	 * Removes the dummy nodes wsci and wscf in the specified graph . 
	 * wsci and wscf are used as control nodes, and wsci is predecessor
	 * to all "input" nodes and wsci is successor to all "output" nodes.
	 *
	 * @param  graph the graph to be modified with the control nodes
	 */
	public static void removeControlNodes(Graph<Service, Number> graph) {
		Service [] services = graph.getVertices().toArray(new Service[graph.getVertices().size()]);
		for(Service s:services) 
			if(s.isControl())
				graph.removeVertex(s);
	}
	
	/**
	 * Expands the graph "expanding" according to the graph "original".
	 * If the expansion was successful, the graph "expanding" is modified.
	 *
	 * @param  graph the graph to be modified with the control nodes
	 * @return true if the graph was expanded; otherwise, false
	 */
	public static boolean expandGraph(Graph<Service, Number> original, Graph<Service, Number> expanding) {
		
		int vertexCount = expanding.getVertexCount();
		
		Service [] services = expanding.getVertices().toArray (new Service[expanding.getVertices().size()]);
		for(Service s:services) 
			if(expanding.getSuccessorCount(s) == 0) 
				for(Service successor:original.getSuccessors(s)) 
					if(!successor.isControl())
						expanding.addEdge(original.findEdge(s, successor), s, successor);
		
		return expanding.getVertexCount() > vertexCount;
	}
	
	/**
	 * Gets a vertex by name.
	 * @param <E>
	 *
	 * @param  graph: the graph
	 * @param  name: the name of the vertex to be searched for
	 * @return the vertex with name "name"; otherwise, null
	 */
	public static <E> Service getVertexByName(Graph<Service, E> graph2, String name) {
		for(Service s:graph2.getVertices())
			if(s.getName().equals(name))
				return s;
		return null;
	}
	
	public static List<Data> listIntersection(List<Data> a, List<Data> b) {
		List<Data> common = new ArrayList<Data>(a);
		common.retainAll(b);
		return common;
	}
	
	
	//Using JUNG KNeighborhoodFilter
	public static List<Graph<Service, Number>> getAllSubgraphs(Graph<Service, Number> graph)  {
		
		List<Graph<Service, Number>> subGraphs = new ArrayList<Graph<Service,Number>>();
		
		for(int i=1; i<=graph.getVertexCount(); i++) {
			for(Service node:graph.getVertices()) {
				KNeighborhoodFilter<Service, Number> kn = new KNeighborhoodFilter<Service, Number>(node, i,KNeighborhoodFilter.EdgeType.IN_OUT);
				
				Graph<Service, Number> subgraph = kn.transform(graph);
				
				boolean contains = false;
				
				for(Graph<Service, Number> s:subGraphs) {
					if(s.getVertices().containsAll(subgraph.getVertices()))
						contains = true;
				}
				if(!contains)
					subGraphs.add(subgraph);
			}
		}
		
		return subGraphs;
	}
	
	
	public static <T extends Comparable<T>,K> boolean hasCycles(Graph<T,K> g) throws Exception {
		//L ← Empty list where we put the sorted elements
	    List<T> l = new ArrayList<T>();
	    
	    //edges
	    List<Pair<T>> e = new ArrayList<Pair<T>>();
	    
	    for(K en:g.getEdges())
    		e.add(g.getEndpoints(en));
	    
	    //Q ← Set of all nodes with no incoming edges
	    List<T> q = new ArrayList<T>();
	    
	    for(T n:g.getVertices()) {
	    	boolean incomingEdge = false;
	    	
	    	for(Pair<T> pair:e) {
	    		if(pair.getSecond().compareTo(n) == 0) {
	    			incomingEdge = true;
	    			break;
	    		}
	    			
	    	}
	    	
	    	if(!incomingEdge)
	    		q.add(n);
	    }

	    
	    //while Q is non-empty do
	    while(!q.isEmpty()) {
	        //remove a node n from Q
	        //insert n into L
	    	T n = q.remove(0);
	    	l.add(n);
	        //for each node m with an edge e from n to m do
	    	for (Iterator<Pair<T>> iterator = e.iterator(); iterator.hasNext(); ) {
	    		Pair<T> pair = iterator.next();
	    		//remove edge e from the graph
	    		T m = null;
	    		if(pair.getFirst().compareTo(n) == 0) {
	    			m = pair.getSecond();
	    			iterator.remove();
	    		}
	    	
	            
	            //if m has no other incoming edges then
	    		boolean hasIncoming = false;
	    		if(m != null) {
		    		for(Pair<T> pair2:e) {
		                //insert m into Q
		    			if(pair2.getSecond().compareTo(m) == 0)
		    				hasIncoming = true;
		    		}
		    		if(!hasIncoming)
		    			q.add(m);
	    		}
	    	}
	    }
	  //if graph has edges then
        //output error message (graph has a cycle)
	    //else 
	        //output message (proposed topologically sorted order: L)
	    if(e.size() > 0) {
	    	return true; 
	    }
		return false;
    }
	
	public static void setInputsToNode(Graph<Service, Number> graph, Service service) {
		
		for(Service p:graph.getPredecessors(service))
			service.setInputs(p.getOutputs());
	}
	
	public static Graph<Service, Number> toServiceGraph(Graph<Integer, Number> toConvert) {
		
		Factory<Graph<Service,Number>> graphFactory =
	        	new Factory<Graph<Service,Number>>() {
	        	public Graph<Service,Number> create() {
	        		//return new SparseMultigraph<Integer,Number>();
	        		return new DirectedSparseMultigraph<Service,Number>();
	        		//return new DirectedSparseGraph<Integer,Number>();
	        		
	        	}
	        };
		
		Graph<Service, Number> graph = graphFactory.create();
				
		for(Number n:toConvert.getEdges())
			graph.addEdge(n, 
					new Service(toConvert.getEndpoints(n).getFirst().toString()), 
					new Service(toConvert.getEndpoints(n).getSecond().toString()));
		
		
		return graph;
	}
	
	/**
	 * Generates random input and outputs to services in a graph
	 *
	 * @param  graph: the graph
	 */
	private static Map<String, Boolean> visited;
	private static Graph<Service, Number> graph;
	public static void generateConcepts(Graph<Service, Number> graph) {
		visited = new HashMap<String, Boolean>();
		GraphUtils.graph = graph;
		int id = 0;
		
		for(Service s:GraphUtils.graph.getVertices())
			if(graph.getPredecessorCount(s) == 0) {
				s.addInput(new Data(CONCEPT + id++));
				generateConcepts(s.getName(), id);
			}
		
		
	}
	
	
	public static void generateConcepts(String sName, int id) {
		Service s = GraphUtils.getVertexByName(graph, sName);
		
		if(GraphUtils.graph.getSuccessorCount(s) == 0 
				&& (s.getOutputs() == null || s.getOutputs().size() == 0))
			s.addOutput(new Data(CONCEPT + id++));
		else
			for(Service succ:GraphUtils.graph.getSuccessors(s)) {
				s.addOutput(new Data(CONCEPT + id));
				GraphUtils.getVertexByName(graph, succ.getName()).addInput(new Data(CONCEPT + id++));
				//succ.addInput(new Data(CONCEPT + id++));
				if(visited.get(succ.getName()) == null) {
					visited.put(succ.getName(), true);
					generateConcepts(succ.getName(), id);
				}
			}
	}

	public static void printServices(Graph<Service, Number> graph) {
		for(Service s:graph.getVertices())
			System.out.println(s);
		
	}
	
	/**
	 * Gets the list of inputs of the CWS represented by a graph
	 * @param graph: the graph
	 * @return list of inputs
	 */
	
	/**
	 * Gets the list of outputs of the CWS represented by a graph
	 * @param graph: the graph
	 * @return list of outputs
	 */
	
	public static <E> List<Data> getInputs(Graph<Service, E> graph) {
		List<Data> inputs = new ArrayList<>();
		
		for(Service s:graph.getVertices()) {
			List<Data> c = new ArrayList<>(s.getInputs());
			for(Service p:getPredecessors(graph, s)) {
				c.removeAll(p.getOutputs());
				if(c.size() == 0)
					break;
			}
			inputs.addAll(c);
			
		}
		
		return inputs;
	}
	
	/**
	 * The following methods are utility methods
	 * to cope with the JUNG bug that causes the lost
	 * of references inside a Service object.
	 *
	 * @param  graph: the graph
	 */
	
	
	public static <E> List<Data> getOutputs(Graph<Service, E> graph) {
		List<Data> outputs = new ArrayList<>();
		
		for(Service s:graph.getVertices()) {
			List<Data> c = new ArrayList<>(s.getOutputs());
			for(Service p:getSuccessors(graph, s)) {
				c.removeAll(p.getInputs());
				if(c.size() == 0)
					break;
			}
			outputs.addAll(c);
			
		}
		
		return outputs;
	}
	
	public static <E> List<Service> getPredecessors(Graph<Service, E> graph, Service service) {
		List<Service> predecessors = new ArrayList<Service>();
		
		for(Service p:graph.getPredecessors(service))
			predecessors.add(getVertexByName(graph, p.getName()));
		
		return predecessors;
	}
	
	public static <E> List<Service> getSuccessors(Graph<Service, E> graph, Service service) {
		List<Service> successors = new ArrayList<Service>();
		
		for(Service p:graph.getSuccessors(service))
			successors.add(getVertexByName(graph, p.getName()));
		
		return successors;
	}
	
	public static <E> void setTransactionalProperty(Graph<Service, E> graph, Service s, TransactionalProperty tp) {
		getVertexByName(graph, s.getName()).setTransactionalProperty(tp);
	}
	

}