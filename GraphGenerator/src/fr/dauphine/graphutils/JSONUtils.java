package fr.dauphine.graphutils;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.json.simple.JSONObject;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Pair;
import fr.dauphine.generator.GraphGenerator;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;
import fr.dauphine.service.TransactionalProperty;

public class JSONUtils {
	
	public static JsonObject graphToJson(Graph<Service, Number> graph) {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		
		//build json array of vertices
		 JsonArrayBuilder verticesBuilder = Json.createArrayBuilder();
		 
		 for(Service s:graph.getVertices()) {
			 JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
			 jsonBuilder.add("name", s.getName());
			 jsonBuilder.add("tp", s.getTransactionalProperty().toString());
			 jsonBuilder.add("time", s.getEstimatedExecutionTime());
			 jsonBuilder.add("price", s.getPrice());
			 jsonBuilder.add("reputation", s.getAvailability());
			 jsonBuilder.add("availability", s.getAvailability());
			 
			 //build json array of inputs
			 JsonArrayBuilder inputsBuilder = Json.createArrayBuilder();
			 for(Data d:s.getInputs()) {
				 JsonObjectBuilder jsonBuilderInputs = Json.createObjectBuilder();
				 jsonBuilderInputs.add("name", d.getName());
				 inputsBuilder.add(jsonBuilderInputs);
			 }
				 
			 
			 
			 //build json array of outputs
			 JsonArrayBuilder outputsBuilder = Json.createArrayBuilder();
			 for(Data d:s.getOutputs()) {
				 JsonObjectBuilder jsonBuilderOutputs = Json.createObjectBuilder();
				 jsonBuilderOutputs.add("name", d.getName());
				 outputsBuilder.add(jsonBuilderOutputs);
			 }
				 
			 
			 
				 
			 jsonBuilder.add("inputs", inputsBuilder.build());
			 jsonBuilder.add("outputs", outputsBuilder.build());
			 
			 verticesBuilder.add(jsonBuilder);
		 }
		     
		  JsonArray vertices = verticesBuilder.build();
		  
		  builder.add("vertices", vertices);
		 
		 //build json array of edges
		  JsonArrayBuilder edgesBuilder = Json.createArrayBuilder();
		  for(Number e:graph.getEdges()) {
			  
			  edgesBuilder.add(Json.createObjectBuilder()
		         .add("first", graph.getEndpoints(e).getFirst().getName())
		         .add("second", graph.getEndpoints(e).getSecond().getName()));
			  
		  }
		  
		  JsonArray edges = edgesBuilder.build();
			  
		  builder.add("edges", edges);
		
		return builder.build();
	}

	public static Map<String, Object> stringToMap(String string) {
		Map<String, Object> map =  new HashMap<>();
		
		JsonObject obj = Json.createReader(new StringReader(string))
				.readObject();

		if(obj.containsKey("vertices"))
			map.put("vertices", obj.getJsonArray("vertices"));

		if(obj.containsKey("edges"))
			map.put("edges", obj.getJsonArray("edges"));
		
		
		return map;
	}

	public static Graph<Service, Number> mapToGraph(Map<String, Object> map) {
		Graph<Service, Number> graph = null;
		List<Pair<String>> pairs = new ArrayList<>();

		if(map.get("vertices") != null) {

			JsonArray jsonEdges = (JsonArray) map.get("edges");
			for (int i = 0, size = jsonEdges.size(); i < size; i++) {
				final JsonObject objectInArray = jsonEdges.getJsonObject(i);

				@SuppressWarnings("serial")
				Pair<String> pair = new Pair<>(new ArrayList<String>() {{
					add(objectInArray.getString("first"));
					add(objectInArray.getString("second"));
				}});
				pairs.add(pair);
			}
			graph = GraphGenerator.generateGraph(pairs);
			
			JsonArray verticesJson = (JsonArray) map.get("vertices");
			for (int i = 0, size = verticesJson.size(); i < size; i++) {
				final JsonObject objectInArray = verticesJson.getJsonObject(i);
				Service s = GraphUtils.getVertexByName(graph, objectInArray.getString("name"));
				s.setTransactionalProperty(TransactionalProperty.valueOf(objectInArray.getString("tp")));
				s.setEstimatedExecutionTime(objectInArray.getJsonNumber("time").doubleValue());
				s.setPrice(objectInArray.getJsonNumber("price").doubleValue());
				//TODO
				//s.setReputation(Double.parseDouble(objectInArray.getString("reputation")));
				s.setAvailability(objectInArray.getJsonNumber("availability").doubleValue());
			
				//inputs and outputs
				
				JsonArray inputs = objectInArray.getJsonArray("inputs");
				for (int j = 0; j < inputs.size(); j++) {
					final JsonObject dataObject = inputs.getJsonObject(j);
					s.addInput(new Data(dataObject.getString("name")));
				}
				JsonArray outputs = objectInArray.getJsonArray("outputs");
				for (int j = 0; j < outputs.size(); j++) {
					final JsonObject dataObject = outputs.getJsonObject(j);
					s.addOutput(new Data(dataObject.getString("name")));
				}
			}

		}
		return graph;
	}

	public static Graph<Service, Number> jsonToGraph(JSONObject obj) {
		return mapToGraph(stringToMap(obj.toString()));
		
	}
	
} 
